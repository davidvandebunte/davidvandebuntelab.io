#!/usr/bin/env sh
set -x -o errexit -o nounset

if [ -z ${GITLAB_CI+x} ]; then
    echo "Building locally"

    ./scripts/install-bazelisk
    ./bazelisk build //jb:cache_tar
else
    # BOTH global.db and the directory it lives in must be writable. It's not clear why they are
    # locally writable by default, not in GitLab CI/CD. See:
    # - https://stackoverflow.com/a/3330616/622049
    find jb/jupyter_cache/ | xargs chmod 777

    docker pull $(./packaging/tag)
fi

docker run --name build_jb \
    --env NB_USER="${USER:=xxx}" \
    --mount type=bind,source=$(pwd)/jb/jupyter_cache,target=/jupyter_cache \
    --volume $(pwd)/jb/tar:/home/jovyan/work \
    --entrypoint /bin/bash \
    --workdir /home/jovyan/work \
    $(./packaging/tag) /home/jovyan/work/jb-build
docker cp build_jb:/tmp/_build/html public/executable-notes
docker rm build_jb
