# Select subplan

```
* Bazel succeeded by another artifact-based build system (e.g. Pants): 2027
| * TODO-pp: Which library should you prefer for future probabilistic programming?
|/
| * TODO-fb: Do you want to be a feedback provider (teacher) in general?
|/
| * Is ResNet a Bayesian net? Is it generative?
|/
| * TODO-lstm: Is the LSTM model a useful abstraction?
|/
| * Why can't we add a single variable to e.g. FasterRCNN, like the country code?
|/
| * TODO-dmy: Restore daily/monthly/yearly planning
|/
| * TODO-cif: Combine `implement-feature.md` and `investigate-root-cause.md`: +2h
|/
| * TODO-nbc: What is a "Naive Bayes Classifier" in your own words?: +1d
|/
| * TODO-ddc: When should you deduplicate code?
|/
| * Organize notes: refactor-code.md: +1h
|/
| * Organize notes: design-experiments.md: +3h
|/
| * TODO-tmm: Does "To Mock a Mockingbird" provide useful abstractions?
|/
| * TODO-bf: Why is "but-for" causation not enough?: +1d
|/
| * TODO-ce: Is "cross-entropy" a useful abstraction?
|/
| * TODO-ngl: Is there a better way to avoid neglect than reminders?
|/
| * TODO-nlp: Do you prefer NLP problems?
|/
| * Policy gradient methods are superseded by another method: 2025
| * TODO-pg: Are "policy gradient" methods a useful abstraction?: +3d
|/
| * TODO-unsup: Do you prefer unsupervised to supervised learning?
|/
| * TODO-aq: How do you answer a question?
|/
| * TODO-cnns: Will attention replace CNNs?
| * TODO-rib: When should you reduce inductive bias?
|/
| * TODO-top: How can topological concepts help you interpret models?
| * TODO-cycl: How is cyclomatic complexity measured?: +8h
|/
| * TODO-idb: How does one quickly identify the bottleneck in a computer program?
|/
| * TODO-ef: Automate expanding focus in training
|/
| * Add a vim/neovim plugin for GhostText. How many significant edits do you make to Wikipedia and SE?
|/
| * TODO-mpd: Move pip and conda dependencies to notebooks
|/
| * TODO-vi: Why are "variational inference" methods so popular?
|/
| * TODO-dss: How would you define "semi-supervised" learning?
|/
| * TODO-pgm: Convert define-generative-model.md to an opinion piece
|/
| * TODO-rb: Move read-book.md to evaluate-pedagogical-tool.md
|/
| * TODO-dr: Should you reduce the dimensionality of your inputs?
|/
| * TODO-up: What are the "universal properties" of map, fold, and filter?: +2w
| * Target: TODO-catt: What's a simple high-level summary of category theory?
|/
* TODO-dagl: Draw a DAG with clickable links: 2022-09-05 15:00
* TODO-wwv: Why do we need a $W_V$ matrix? 2022-08-21 15:00
* TODO-kqs: Do the K and Q matrices learn to project to the same space?: 2022-08-20 13:00
* TODO-kqv: What exactly are keys, queries, and values in attention mechanisms?: 2022-08-19 16:00
* Partial: What exactly are keys, queries, and values in attention mechanisms?: 2022-08-17 9:15
* How is attention a form of soft weights?: 2022-08-14 9:15
* Should you always record dependencies?: 2022-08-12 15:40
* What is the difference between self-attention and attention?: 2022-08-12 13:45
* How should you control your attention?: 2022-08-12 13:00
* Explore domain: improve-add-attention-mechanism.md: 2022-08-12 13:00
* Why do you wear headphones with white noise? See `narrow-focus.md`.
* TODO-svd: How can SVD be used to reduce dimensionality?: 2022-06-20 14:00
* TODO-pfn: Why do you record any links you share?: 2022-06-20 9:50
* Organize notes on attention: 2022-06-16 20:00
* What should you do next?: 2022-06-14 15:30
* Add search-the-web.md: 2022-06-14 12:45
* Add add-attention-mechanism.md: 2022-06-13 20:00
* What should you do next? How do you plan what to do next?: 2022-06-10 15:45
* Create draft document for estimate-subplan-weight.md: 2022-06-10 13:15
* Organize notes: ML-related documents: 2022-06-10 12:30
* Organize notes: improve-estimate-subplan-weight.md: 2022-06-10 10:00
* Add explore-domain.md: 2022-06-09 17:00
* Organize notes to prepare for incorporating curiosity and exploration: 2022-06-08
* What should you do next? How do you plan what to do next?: 2022-06-08 11:00
* Add define-generative-model.md: 2022-06-06: 17:40
* How is planning a week different than a day?: 2022-06-06 10:15
* What about 9.8 TODo? How do you pick what to do next?: 2022-06-06 19:00
* TODO-rd: Should you retrospect today?
* TODO-draft: Why publish "draft" articles?
* TODO-ur: Why do you only upgrade packages before a reboot?
* TODO-pri: When do your prefer the term personal to private?
* TODO-state: Why include a "State" section under every "Cost" section?
* TODO-c: Why convert completed TODo to retrospective TODo (in Training Data)?
* TODO-q: Why do you create all TODo in the form of a question?
* TODO-meet: How often should you meet with your feedback providers?
* TODO-ant: Why anticipate other's favorite part of your notes?
* TODO-ask: Why ask for feedback on published notes?
* TODO-mach: Why prefer a specific machine name to the term "local machine"?
* TODO-vn: How do you create a new file in the same directory in `vim`?
* TODO-mt: Why merge auto-committed tags to keep them?
* TODO-arx: Why prefer the term "archive" to delete (e.g. in commit messages)?
```
