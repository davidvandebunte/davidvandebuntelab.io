---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Share to Stack Exchange

+++

## Question

+++

You'll need to reformulate your comments as a question. Is this so bad? You can fit almost any content you want to share into the question format.

+++

## Promote

+++

If you already have content on your own website, you are allowed to link to it from an SO answer; see [What is the policy for linking to your own blog in your answers? - Meta Stack Overflow](https://meta.stackoverflow.com/questions/254280/what-is-the-policy-for-linking-to-your-own-blog-in-your-answers). In some cases this may be required e.g. if you want to share the output of significant computation.

+++

## SVGs

+++

Part of the reason that Wikipedia authors don't post to SE is that we often want to be able to edit SVG files later; on SE you can only post PNG files. As an alternative, include the name of `.svg` file that you're converting to PNG in the Markdown as an HTML comment i.e. `<!--- filename.svg --->`. If you ever want to edit it more, you'll have the name there.

+++

## Reputation

+++

A high score on different SE sites looks good on a resume (you should link all your social profiles on your website, including emphasizing which SE sites you are most active on). You'll also reach more people; let them do the SEO for you.

+++

## Stale

+++

The content on Stack Exchange is much more likely to go stale than e.g. Wikipedia because it doesn't have as many maintainers, and because no one can possibly maintain votes. You can remove votes you've made that you no longer agree with yourself, but no one else is going to do that. You could even downvote bad answers, but that takes reputation.

See for example [LaTeX Editors/IDEs](https://tex.stackexchange.com/questions/339/latex-editors-ides), with the new tool `vimtex` way at the bottom. It simply hasn't had time to get votes because this question is so old; however a large number of people would prefer to it options with more votes.
