---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# History of mathematics

+++

See [History of mathematics](https://en.wikipedia.org/wiki/History_of_mathematics). For a briefer overview of some of the actors in that history:

+++

![x](math-history.svg)

+++

# Value

+++

## One definition

+++

Ideally, the history of mathematics will provide us with words to define concepts in such a way that we don't need to reinvent words for something that has already been created. Just as humans forget, we would also communally forget concepts that weren't useful as part of the natural loss of history.

+++

## If you can't build it

+++

If you can't build it, then you don't understand it. There's always at least one option to understand how something was built: look at the history. This means that you'll end up learning mathematical history naturally. It also means that certain people are going to be stuck in human memory long-term because they were the first to build something. If you can't be the first to build something, you can also be someone who popularizes a not-well-known method of building it. Eventually you may replace the "first" person to do something (if they were really even the first to do so).

+++

## Bottom and history

+++

It may be that there's no bottom. But for you, and any book, and computer code, there's a bottom. It's a product of history, but history matters. It literally determines the language we’re speaking and thinking in (English).

+++

# Cost

+++

## Author existence

+++

Does it matter if Euclid was a real person? He's defined by his work (the Elements) and he has been effectively reduced (compressed) to what he wrote and what was written about him. His name has essentially become an anchor to his work.

With this assumption, you also don't care about Gauss, but [Disquisitiones Arithmeticae](https://en.wikipedia.org/wiki/Disquisitiones_Arithmeticae). If you're interested in someone in particular, find an [enumerative bibliography](https://en.wikipedia.org/wiki/Bibliography#Enumerative_bibliography) centered on them (see e.g. [Leonhard Euler § Selected bibliography](https://en.wikipedia.org/wiki/Leonhard_Euler#Selected_bibliography)). See also [WTW for an author's entire works](https://www.reddit.com/r/whatstheword/comments/2klsnq/wtw_for_an_authors_entire_works_like_a_musicians/).
