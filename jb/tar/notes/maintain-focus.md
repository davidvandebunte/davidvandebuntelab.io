# Maintain focus

## Estimate value

Deliver value in terms of improvements to whatever state you were previously focusing on changing.
Generalizes many processes. To make your mind attend to the same:
- Large scale goal, see [](./set-subgoal.md).
- Section of your notes, see [](./organize-notes.md).

% Code: `refactor-code.md`

### Sustainable screen space consumption

[cl]: https://en.wikipedia.org/wiki/Cognitive_load

Your short-term memory can only attend to so much (also referred to as your [Cognitive load][cl]).
If you open more text editor or web browser tabs, you are decreasing your ability to focus on the
set of windows you already have open; see [Attention - Exogeneous and endogeneous orienting](
https://en.wikipedia.org/wiki/Attention#Exogenous_and_endogenous_orienting). That is, the windows
you have open will either need to be forgotten (totally acceptable, just close them) or they will
eventually jump back into your focus through exogeneous orienting when you e.g. need visual space on
your computer monitors and you have to go through all your windows. You can't focus on anything new
unless you have monitor space.

If you don't stay safely under your visual space limits over the course of e.g. several days, you'll
eventually hit an idea spike, run out of space, and then potentially lose valuable thoughts by
dumping them to an unorganized location you never return to. On the other hand, if you don't give
yourself enough context when working on a problem (from e.g. several days of memory), you won't
think as deeply into your problem and achieve greater insights (better deep rather than shallow
internal representations).

### Extend mental vision

Let's say you're able to recall something you're read in the last hour with probability 0.8, in the
last day 0.6, and the last week 0.4. These are like levels of memory (cache, RAM, disk) not only in
speed of recall but in the likelihood you will be *able* to recall. When you recall the past day or
week, you'll build more mental connections to help future recall. When you switch focus, you're
going to be able to recall many things from the last week or day that you simply won't need to
recall (inefficient, fewer connections built).

As we get older we're able to recall farther into history than a child can; we need to because we
have more history (we can recall to e.g. ten years ago). Perhaps we trade off this ability for
plasticity. We focus on training an extractor network separately from a detector network (for
example) to get a better result overall. So we prioritize different areas of the network; we have to
because of our limited memory.

## Design test

Attend to the same goal or task. Analogous to "Sustained" or "Selective" attention in the model of
Sohlberg and Mateer; see [Attention](https://en.wikipedia.org/wiki/Attention). To analogize to the
work of training a net, continue backpropagation on the same training example. Contiguous connected
thoughts are a way to measure the degree to which you're in focused work, but it's hard to make this
numerical. When you're in focused work, you sometimes do not need to eat until you're quite hungry
(a more measurable effect).

## Estimate cost

Close your eyes; this avoids exogeneous orienting. You can get this looking out the window of your
car while driving as well (in general, look at something uninteresting).

### Take notes

Previous time in a "thinking fast" mode makes it hard to focus. The more you think slow, the better
you get at it. Take plain text notes if possible; see [](./email-yourself.md) about how emailing
yourself only extends feelings of distraction (even when you can't take plain text notes). Arguing
is a good way to get into focused work (e.g. with your family or any coworker). If you don't even
have your phone or a person to talk to, use your fingers not only for mental lists but to help you
remember what you were thinking about.

Pride and day dreaming is one way to get absolutely nothing done (monologuing). Consider instead
what you've done rather than what you're going to do, and what you've done that others respect
rather than what you are personally proud of.

### Work/life sharing

You shouldn’t be reading a book on one topic and doing a computer project on something totally
unrelated. It's more important to work on the same thing continuously than to be doing it all at the
computer (a phone is OK if it extends work on a desktop computer). If you already have topics that
are focal/important at work, you should seriously consider continuing to work on those in your free
time.

### Search your web browser

Do a search for a word on your existing browser tab. It's likely what you see will be more
appropriate than a global web search.

Use `%` to search open tabs rather than opening a new one. See [Search open tabs in Firefox |
Firefox Help](https://support.mozilla.org/en-US/kb/search-open-tabs-firefox). When you use this in a
new tab, the new tab is automatically removed when you select an existing tab. You can start or end
your search with this character; see other options in [Changing results on the fly | Firefox
Help](https://support.mozilla.org/en-US/kb/address-bar-autocomplete-firefox#w_changing-results-on-the-fly).

Simply click on links rather than Ctrl-click to avoid excessive tabs, as well. Use Ctrl-click when
you want to expand your scope (see [](./expand-focus.md)), effectively adding a new goal.

### Zoom out

It helps to be able to zoom in more as you get more invested in a webpage. This is similar to how
math (e.g. numbers) and code is a more compressed version of natural language; variable names are
often too short. What you gain is the ability to attend to more at once. Prefer to do this on a
per-site basis to avoid global tweaking; site owners have likely already tweaked their context to
browser defaults. For now you "Zoom Text only" to avoid tweaking the site owner's image sizes. See
[Font size and zoom - increase the size of web pages | Firefox Help](
https://support.mozilla.org/en-US/kb/font-size-and-zoom-increase-size-of-web-pages?redirectslug=Page+Zoom&redirectlocale=en-US)
and [Change the fonts and colors websites use | Firefox Help](
https://support.mozilla.org/en-US/kb/change-fonts-and-colors-websites-use).

Your zoom settings are site-specific, unfortunately (not tab-specific). That is, if you set your
zoom to 90% on one Wikipedia page it will apply to all Wikipedia pages (though not other websites).
You've zoomed way in by default on your own website (it's easy for you to consume). Your text editor
should already be "zoomed" to let you fit as much as possible on a screen.

% See TODO-dr for other thoughts on how compression helps you attend to more at once.

### Estimate first

You should be able to provide some estimate of how hard a question is to answer before you expand
your focus. Is this a quick search or something that will take an hour? Said another way, you should
always have an estimate for when you will close a tab when you open it. You can see open tabs as
open tasks or questions that you close when the task/question is complete, and therefore opening
opening a tab as an expansion in scope (necessary or not). Know which tab you are working on; the
more you have the harder this gets.

#### More than 5 minutes

Take notes when you want to attend to more weights deeper into your recent thoughts. Notes force
your focus on a certain train of thought by making it real on your screen (and in front of your
eyes, regularly forcing the context back into focus through exogeneous orienting). It's a way to
assist your own limited ability to think deeply into your current context. You can take plain-text
"notes" in a browser tab (e.g. a Jupyter notebook when you're editing mathematics), or by leaving
tabs unclosed (if so, move important tabs right where new tabs appear).

To take notes is not only to serialize your thoughts long-term. It's also to serialize them
temporarily (e.g. over the next 5 minutes) to force you into focus. Is this a good practice? It
keeps you on focus, though it may make answering verbal questions (keeping your train of thought)
difficult because you'd be missing your crutch.

Notes are also good evidence that you know the exact question you want, and that you're going to use
good searching tools e.g. [](./search-the-web.md) to answer the question (dependency). It also
indicates that you've thought enough about the question to believe that it is a strict dependency;
you've read alternative explanations and they do not solve the problem. There's always more than one
way to solve a problem (understand). You'll also want the question written down in case you get
interrupted somehow.

Try to answer the question from your own notes (like an open note test) before moving on to a web
search. Why? You do have a preference for e.g. Bayesian to Frequentist statistics. More importantly,
you may be able to skip the step of web searching if you already have already collected good
pedagogical resources on a topic. You'll be able to learn much faster from resources you've already
evaluated for quality and skimmed.

If you're working from both a primary and supplementary article, give more space to the
supplementary article by putting it in a different browser window and a different monitor. It's not
easy to rapidly switch back and forth between two tabs and still understand, especially if you need
to open more tabs once in a while. See [shortcut key to detach a tab in firefox (move tab to new
window) - SU](https://superuser.com/a/1514487/293032); to re-attach use a similar strategy.
Alternatively, always keep two Firefox windows open, one leaning SE and one leaning Wikipedia. Most
of the time, however, you should know your primary resource and follow it.

#### More than 1 hour

In some sense this is a "little failure" because your own language was not enough (with the
resource) to solve the problem. When you run into a "little success" you naturally go through
closing tabs and windows that are no longer relevant. You should do the same for little failures;
serialize what you've learned so you can add a dependency (unfortunately expanding scope) i.e.
construct an abstraction. If the estimate is large, then in order to avoid spilling notes you will
need to narrow your focus to some degree; see [](./narrow-focus.md).

##### Save the dependency

You've discovered a dependency if you can't answer a question without opening another tab. There may
be another way to construct your new abstraction without, and in the short-term you should usually
look for alternative solutions. Even if you find another way, however, to understand the problem
from this additional perspective requires this dependency.

Why save the dependency, even if you don't fill it in now? You'd ideally like to understand the new
abstraction from as many perspectives as possible so you don't forget it and so that you can quickly
relearn the concept from e.g. a different, shorter perspective. If you can rederive the result with
this new dependency, you may eventually be able to reduce duplication in your mental networks if the
new dependency ends up being widely useful.

The new dependency may be critical; there may be no other way to build your new abstraction without
it. You should treat this as adding a subgoal (see [](./set-subgoal.md)), going back to planning,
and then targeting that new subgoal.

Therefore, at the least, add the dependency to a planning git graph and a link/keyword in the notes
where you are discussing the problem. The link/keyword is helpful so you can find the "missing"
concept later when you are grepping for e.g. mathematical keywords that may be worth learning about
(see comments in [](./expand-focus.md)). The planning graph is helpful so you remember why the
concept would have been useful if you don't ever `grep` as you should. A single question may not be
enough to motivate returning to the dependency. See comments in the doubly-linked
[](./flatten-plan.md) about retaining dependencies.

Another major reason to save off (write down) missing dependencies is it motivates completion. You
do not need to understand all material from every possible perspective (e.g. frequentist
interpretations, in some cases); by saving off the missing perspectives you will feel you are able
to move on. For a SE answer, this is like adding a private comment, one that is not appropriate for
the public domain because you haven't done sufficient research to answer it on your own (but also
don't want to). Said another way, you're stripping out minor alternative goals. The result (in your
notes) is a mix of a reminder TODo and unresolved questions.

### Say no

Some people spend all their time making work focal, so it becomes more important in other people’s
minds, so it gets done. Use this strategy when you take the time to analyze work and discover it is
high weight; send an email with your analysis to a large group of people just to make it focal
enough that someone does it or you get the green light to assign it to someone. See [Pre-Suasion, A
Revolutionary Way to Influence and Persuade - Google Play Books](
https://play.google.com/books/reader?id=RmOdCwAAQBAJ&printsec=frontcover&pg=GBS.PA367.w.2.0.161).
See also [](./handle-interruption.md)

There are many examples of this in the workplace. In my experience, when you plan a meeting, people
will come; I've had 40 people show up to a meeting even though most were optional when I was
expecting only a few. The issue is that in many environments developers feel like they can't say no,
or they won't look like a team player. It's incredibly easy to abuse this feeling; instead
developers need to develop priors about the quality of the content that every individiual coworker
typically presents (and be willing to say no).
