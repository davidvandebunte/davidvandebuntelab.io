---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# 6. Subgroups

+++

Compare [Subgroup](https://en.wikipedia.org/wiki/Subgroup).

+++

It's not clear if the author's definition of "regular" is anything but a completely custom (and informal) description specific to the author. The author does provide a reference to a more technical definition in the footnote on pg. 98, however.

+++

## 6.4 Cosets

+++

Compare [Coset](https://en.wikipedia.org/wiki/Coset).

See also, from [Affine space](https://en.wikipedia.org/wiki/Affine_space):
> Any [coset](https://en.wikipedia.org/wiki/Coset "Coset") of a subspace V of a vector space is an affine space over that subspace.

+++

## 6.4 Lagrange's theorem

+++

Compare [Lagrange's theorem (group theory)](https://en.wikipedia.org/wiki/Lagrange%27s_theorem_(group_theory)).
