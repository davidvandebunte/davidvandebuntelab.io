---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# 2. What do groups look like?

+++

## 2.4 Cayley diagrams

+++

See also [Cayley graph](https://en.wikipedia.org/wiki/Cayley_graph).

+++

## 2.5 A touch more abstract

+++

Another way to describe what we're doing here is in the language of syntax and semantics. Everything we do must necessarily map to human language and our human problems. This section demonstrates how the same syntax (mathematical model) can have different semantics; it provides two examples of semantics for the same group. We seek to "generalize" both of these problems into one so we can study both of them at once.
