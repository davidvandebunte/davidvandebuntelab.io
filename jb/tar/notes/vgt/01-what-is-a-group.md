---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# 1. What is a group?

+++

## 1.1 A famous toy

+++

See also [Rubik's Cube group](https://en.wikipedia.org/wiki/Rubik%27s_Cube_group).

+++

## 1.2 Considering the cube

+++

The language in this chapter is alluding to [Group action](https://en.wikipedia.org/wiki/Group_action), a concept that we'll see later in the book.

+++

The "action" language is in fact quite appropriate; if you're studying group theory with an application to robotics in mind then you can see the possible actions the robot can make in terms of moving limbs as actions in the [General linear group](https://en.wikipedia.org/wiki/General_linear_group).
