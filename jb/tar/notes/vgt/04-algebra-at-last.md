---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# 4. Algebra at last

+++

## 4.3 Multiplication tables

+++

See also [Cayley table](https://en.wikipedia.org/wiki/Cayley_table).

+++

## 4.4 The classic definition

+++

Compare to [Group (mathematics)](https://en.wikipedia.org/wiki/Group_(mathematics)).
