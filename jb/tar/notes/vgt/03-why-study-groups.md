---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# 3. Why study groups?

+++

See [VGT § Errata](http://web.bentley.edu/empl/c/ncarter/vgt/errata.html) for known issues in this chapter.

+++

## 3.1 Groups of symmetries

+++

This section discusses [Symmetry group](https://en.wikipedia.org/wiki/Symmetry_group). See also [Symmetry (disambiguation)](https://en.wikipedia.org/wiki/Symmetry_(disambiguation)), in particular [Symmetry in mathematics](https://en.wikipedia.org/wiki/Symmetry_in_mathematics):

> Symmetry is a type of [invariance](https://en.wikipedia.org/wiki/Invariant_(mathematics) "Invariant (mathematics)"): the property that a mathematical object remains unchanged under a set of [operations](https://en.wikipedia.org/wiki/Operation_(mathematics) "Operation (mathematics)") or [transformations](https://en.wikipedia.org/wiki/Transformation_(mathematics) "Transformation (mathematics)").

+++

When the author says "similar" in Definition 3.1 he means it in a technical sense; see [Similarity (geometry)](https://en.wikipedia.org/wiki/Similarity_(geometry)). You'll often see "L" used to make this check easier, as in [Dihedral group of order 6](https://en.wikipedia.org/wiki/Dihedral_group_of_order_6).

+++

In this section, the invariant we are pursuing is the occupation of ambient space. Some things change and some stay the same; it's not like rotating a molecule means "nothing changed" but that nothing changed regarding the metrics we care about. What does change in this context is e.g. the labeling we apply to parts of the object, which are considered not part of the ambient space. If we were to write them on the objects with marker, we would imagine the marker as taking no space.

+++

### 3.1.1 Shapes of molecules (⚠)

+++

See the [VGT Errata](http://web.bentley.edu/empl/c/ncarter/vgt/errata.html) for Figure 3.5.

+++

### 3.1.3 Art and Architecture

+++

See also [Glide reflection](https://en.wikipedia.org/wiki/Glide_reflection). The author specifically mentions the seven frieze groups of [Frieze group § Descriptions of the seven frieze groups](https://en.wikipedia.org/wiki/Frieze_group#Descriptions_of_the_seven_frieze_groups) and the seventeen wallpaper groups of [Wallpaper group § The seventeen groups](https://en.wikipedia.org/wiki/Wallpaper_group#The_seventeen_groups).

+++

## 3.2 Groups of actions

+++

The misspelling of "Defintion" is not mentioned in the errata.

+++

## 3.3 Groups everywhere

+++

See also [Group theory § Applications of group theory](https://en.wikipedia.org/wiki/Group_theory#Applications_of_group_theory).
