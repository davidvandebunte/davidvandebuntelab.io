# About


## Tags


We'll use these tags throughout the book:


### 📑: Available answer


A 📑 indicates an answer to a question is available as part of the resource (e.g. the end of a [textbook](https://en.wikipedia.org/wiki/Textbook)).

Answers are also usually included in this Jupyer Book, partially because this text is close to an answer key already (it'll often fill in missing dependencies). A major benefit to providing these solutions is it gives an opportunity to comment or fix the author's answers (based on in particular, the errata).

One reason to provide solutions inline is to avoid the [Split attention effect](https://en.wikipedia.org/wiki/Split_attention_effect). Other times we hide answers to make it easier for readers to start with hints and move on to the complete solution.


### ⚠ (or ⚠️): Known issue


We'll mark questions and sections with a ⚠ if one should reference the errata before starting. We won't do this for typos but only non-trivial problems where it would be helpful to see the errata before reading or trying to answer a question.


### 🕳️: Appendix issue


Sometimes both a question and its answer have issues (the author was having a bad day). We'll use 🕳️ rather than 📑 when an answer has issues so we check the errata when we finish the question.


### 🔨: Work in progress


Some questions don't have complete answers, usually because we decided it didn't seem valuable to finish answering the question (there was not enough insight to be gained given the apparent cost).


### 📌: Unresolved issue


This tag indicates that some content still appears to be incorrect, and the issue is not mentioned in the errata.


## Commenting


Jupyter Book has excellent tools for [Commenting and annotating](https://jupyterbook.org/en/stable/interactive/comments.html) that are not enabled in this book to avoid spam. Please add suggestions to [Suggestions: Executable Notes](https://docs.google.com/document/d/1K7rpdT1A84U5z5p_cg9eG6R25as6ty09REUTJLYO0YI/edit) if you'd like to remain anonymous, keep your email private, or are not interested in follow-up questions. If you see something, say something! Even misspellings hurt readability.

If you're more interested in a conversation, use the "share" button on your device and email me at davidvandebunte at Google's mail. I'm always grateful for feedback, including questions.


% To get notifications on changes to this document, see [Google Docs Editors Help](https://support.google.com/docs/answer/91588?hl=en&co=GENIE.Platform%3DDesktop).


## Content license


All content is [GPL v3](https://www.gnu.org/licenses/gpl-3.0.html).


## Versioning


See the git history of [davidvandebunte.gitlab.io · GitLab](https://gitlab.com/davidvandebunte/davidvandebunte.gitlab.io) for older versions of articles.


% Someone could fork this repo if they really wanted a static version of an article.
