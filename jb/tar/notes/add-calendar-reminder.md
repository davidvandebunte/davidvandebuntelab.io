# Add calendar reminder

Email reminders are one way to intentionally form a new habit, if you've ruled out better
alternatives.

# Cost

## Forced practiced

One alternative is forced practice. Force yourself to practice rather than passively read. For
example, can you disable keys to force yourself to learn some other keys?

## Constant editing

Every time you see a reminder you should be able to make it a little shorter; reminders should
always be links to a plain text document. Would it be better to see these reminders as you come to
the topic, though? Requiring practice through a reminder is much less natural (less self-motivated).

## Memory Loss

[mt]: https://www.inc.com/nate-klemp/want-to-improve-your-memory-a-decade-long-stanford-study-suggests-you-should-stop-doing-this-1-thing.html

See [Want to Improve Your Memory? A Decade-Long Stanford Study Suggests You Should Stop Doing This 1
Thing | Inc.com][mt]. The one thing is multitasking. For example, answering your emails, texts, etc.
while in a meeting.

## Writing

Quiz yourself so you're forced to recall the answer. This is the "forced" alternative to letting
nature take its course. In many cases you'll also develop a habit naturally as you are repeatedly
quizzed by nature and your actions.

## Incorrect prioritization

If you have a weekly reminder and don't want to make it less frequent, it's by definition more
important to you than less frequent reminders. Publish it before other reminders; set the TODo
priority to the reciprocal of the frequency (in days) to remain consistent. Still, it's hard to have
a global view of all your calendar items and set all their reminder frequencies inversely
proportional to their current priority.

## Time Cost

Small emails prevent you from getting into focused work faster. When you have a commute it is more
OK to have some small tasks to do on e.g. the train, though if you have a commute you should aim to
come off the train (95% of the time) with zero emails and keep notes. Without a commute, every small
email directly affects how long it takes to get into focused work on a daily basis.

Is the time you’re going to put into reading this reminder justified in terms of VNTE? The E is very
high when a reminder comes every week. It's easier to assess this when reminders are grouped into a
learning document.

## News Feeds

New feeds are addictive, like emails. We are already addicted to short summaries of complicated
topics; we can feel like we understand when we don't (e.g. the monad fallacy).

% See Google Calendar tricks:
% - https://docs.google.com/document/d/1FoaSROXantk3bz2MU2ZjVhZn0_AHxRUcNUvo2yq0OOs/edit.
