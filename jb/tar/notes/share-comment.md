---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Share comment

+++

## Test

+++

Comment on other articles, [StackExchange](https://stackexchange.com/), or by writing an article that is essentially a large comment on another article.

+++

## Value

+++

You'll likely get a response from the original author.

+++

## Cost

+++

Comments can usually be deleted or directed to a smaller audience (even one person).

If you see something, say something.
