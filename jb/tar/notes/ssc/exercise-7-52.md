---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Exercise 7.52

+++

![x](raster/2023-10-25T18-29-14.png)

+++

The one point space has two open sets and could be represented ∅ → {1}. Based on Eq. 7.50 we assign $Ω(∅) = \{∅\}$ and we assign $Ω(\{1\}) = \{∅,\{1\}\}$.

The non-empty set is being mapped to a two object set we can see as the booleans. Therefore we can see any morphism between sheaves (a natural transformation) that targets this sheaf as a function to the booleans.
