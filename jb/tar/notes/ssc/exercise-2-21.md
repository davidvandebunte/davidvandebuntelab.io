---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": []}

# Exercise 2.21

+++

![x](raster/2023-10-27T15-30-42.png)

+++ {"jp-MarkdownHeadingCollapsed": true, "tags": []}

The condition `(a)` holds by the conservation of matter. The condition `(b)` holds because adding
nothing to a reactant leaves you with the reactant as the product. The condition `(d)` holds because
it shouldn't matter what order reactants are added together in a reaction.

It's not clear that `(c)` would hold in all circumstances; adding two reactants may produce a
product that no longer reacts with another reactant that the original reactants may have reacted
with.
