---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": []}

# Exercise 2.93

+++

![x](raster/2023-09-14T21-31-14.png)

+++

We already defined the join for the empty set. For the singleton sets {T} and {F} the join is simply
the same element of the set. For the set {T,F} the join is T.
