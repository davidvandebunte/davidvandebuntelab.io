---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": []}

# Exercise 2.33

+++

![x](raster/2023-10-27T15-50-36.png)

+++

No, because $0 | n = \infty \neq 0$.
