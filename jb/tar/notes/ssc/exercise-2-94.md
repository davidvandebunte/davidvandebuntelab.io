---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": []}

# Exercise 2.94

+++

![x](raster/2023-09-14T21-32-07.png)

+++

Yes; the join of the empty set will be the empty set (the least element). The join of any singleton
set (e.g. $\{\{T\}\}$ or $\{\{F\}\}$ in **Bool**) will be the same element. All sets of subsets will
have a join that is the union of the included subsets.
