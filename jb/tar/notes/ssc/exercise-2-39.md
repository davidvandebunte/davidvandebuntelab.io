---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": []}

# Exercise 2.39

+++

![x](raster/2023-10-27T16-03-32.png)

+++

The $I$ remains the identity because in the original symmetric monoidal preorder we had $I \otimes x
= x$ and $x \otimes I = x$ and nothing about $\otimes$ has changed. Similarly, if $\otimes$ was
associative and commutative it should remain so.
