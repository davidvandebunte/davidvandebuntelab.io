# 3.1. What is a database?


![x](raster/2023-10-28T21-51-45.png)


![x](raster/2023-10-28T21-52-13.png)


![x](raster/2023-10-28T21-53-13.png)


## Exercise 3.3


![x](raster/2023-10-28T21-53-46.png)


In both cases, the count is 5. This is not a coincidence because every non-ID column in the two
tables represents a reference (internal or external, of which there are 3 and 2 in this case), and
every arrow in the graph represents a reference (internal or external) as well. Notice the 5 labels
on the 5 arrows in the graph match the 5 column names in the tables.


```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-42-10.png)
```


![x](raster/2023-10-28T21-55-20.png)


![x](raster/2023-10-28T21-55-48.png)


![x](raster/2023-10-28T21-56-10.png)


![x](raster/2023-10-28T21-56-41.png)


The introduction to [Category theory](https://en.wikipedia.org/wiki/Category_theory) provides the
same list of fundamental concepts behind category theory.


![x](raster/2023-10-28T21-58-39.png)


![x](raster/2023-10-28T22-00-27.png)
