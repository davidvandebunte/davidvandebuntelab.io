---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Exercise 2.45

+++

![x](raster/2023-10-27T16-18-43.png)

+++

To answer `1.`, see the same question in Exercise 2.31.

To answer `2.`, we simply need to guess and check different kinds of functions for the monoidal
monotone. An answer that works is $f(p) := n^p$ where $n$ is any integer (e.g. 2).

To answer `3.`, see nearly the same question in Exercise 2.5.
