---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# 3.2. Categories

+++

![x](raster/2023-08-12T17-16-18.png)

+++

For a similar definition, see [Category (mathematics)](https://en.wikipedia.org/wiki/Category_(mathematics)). It excludes (iii), but makes a note that some authors include it. This page also provides a long list of examples, similar to what this section will do.

As noted in [Morphism](https://en.wikipedia.org/wiki/Morphism), some people also prefer to use "hom-class" for what is called "hom-set" here.

Thinking only in terms of sets and functions is limiting. What if all the items in your set share some property that you want maintain (or transform) as part of the functions associated with it? For example, groups have several definining properties and group homomorphisms maintain them. This is where category theory begins.

You can come to nearly the same perspective by thinking in terms of the elements/examples in the set you care about as training examples. What properties do the elements have in common that makes them belong to the same "set" so to speak? If you're going to do any work on your training examples then you need to understand these properties and be careful about how you maintain or transform them. That is, you can't just apply any functions to them.

+++

![x](raster/2023-08-12T17-21-51.png)

+++

## 3.2.1. Free categories

+++

![x](raster/2023-10-29T21-52-18.png)

+++

Compare to [Free category](https://en.wikipedia.org/wiki/Free_category).

+++

### Exercise 3.9.

+++

![x](raster/2023-10-29T21-52-45.png)

+++

The *unitality* condition is satisfied because concatenating (composing) with identities at any
vertex does nothing. The *associativity* property is satisfied because concatenating arrows should
lead to the same path regardless of which arrows you concatenate first.

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-46-19.png)
```

+++

### Exercise 3.10.

+++

![x](raster/2023-10-29T21-55-53.png)

+++

![x](raster/2023-10-29T21-56-33.png)

+++

For `1.`:

![x](ssc-exercise-3-10.svg)

For `2.`, the identities only show up in the resulting table as an identity matrix where the same
identities line up. The identities can only be concatenated with themselves.

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-48-12.png)
```

+++

### Exercise 3.12.

+++

![x](raster/2023-10-29T21-56-15.png)

+++

The category **1** would have only object v₁ for the one vertex, and one morphism id₁ for it's
identity morphism.

The category **0** would have the empty set for both its collection of objects and all hom-sets
between objects (of which there are none).

You'd have `n` identify morphisms, `max(0, n-1)` length-one morphisms, `max(0, n-2)` length-two
morphisms, etc. The pattern may be more familiar when the calculations are written out:

```
|hom(0)| = 0
|hom(1)| = 1
|hom(2)| = 2 + 1
|hom(3)| = 3 + 2 + 1
|hom(4)| = 4 + 3 + 2 + 1
```

This is an arithmetic series; the formula for the nth term is `n(n+1) / 2`.

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-48-38.png)
```

+++

### Example 3.13.

+++

![x](raster/2023-08-19T14-36-34.png)

+++

### Exercise 3.15.

+++

![x](raster/2023-10-29T21-57-20.png)

+++

The addition of the two numbers.

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-51-00.png)
```

+++

## 3.2.2. Presenting categories via path equations

+++

![x](raster/2023-10-29T22-00-48.png)

+++

We present categories with a set of "generators" and equations, analogous to the [Presentation of a group](https://en.wikipedia.org/wiki/Presentation_of_a_group).

+++

### Exercise 3.16.

+++

![x](raster/2023-10-29T22-01-20.png)

+++

The ten paths are A, B, C, D, f, g, h, i, hf, and ig. The hf and ig paths are parallel. The f and i
paths are not parallel.

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-53-07.png)
```

+++

![x](raster/2023-10-29T22-02-32.png)

+++

### Exercise 3.17.

+++

![x](raster/2023-10-29T22-03-32.png)

+++

A, B, C, D, f, g, h, i, j

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-53-39.png)
```

+++

![x](raster/2023-10-29T22-05-14.png)

+++

### Exercise 3.19.

+++

![x](raster/2023-10-29T22-06-24.png)

+++

```
z
s = z;s = s;z
s;s = s;s;s;s = z;s;s = s;s;z
s;s;s = z;s;s;s = s;s;s;z = s;s;s;s;s
```

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-54-03.png)
```

+++

The author's answer seems wrong; he claims the morphisms are "shown below" and then reproduces the same drawing from Example 3.18.

+++

## 3.2.3. Preorders and free categories: two ends of a spectrum

+++

![x](raster/2023-10-29T22-13-25.png)

+++

![x](raster/2023-10-29T22-13-56.png)

+++

![x](raster/2023-10-29T22-14-33.png)

+++

### Exercise 3.21.

+++

![x](raster/2023-10-29T22-15-11.png)

+++

G₁ would need f = g. G₂ would need id = f. G₃ would need hf = ig. G₄ would not need anything.

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-56-16.png)
```

+++

![x](raster/2023-10-29T22-16-06.png)

+++

### Exercise 3.22.

+++

![x](raster/2023-10-29T22-16-57.png)

+++

The discrete preorder with one element.

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T16-57-37.png)
```

+++

![x](raster/2023-10-29T22-17-36.png)

+++

## 3.2.4. Important categories in mathematics

+++

![x](raster/2023-10-29T22-20-07.png)

+++

![x](raster/2023-10-29T22-20-25.png)

+++

![x](raster/2023-10-29T22-24-23.png)

+++

### Exercise 3.25.

+++

![x](raster/2023-10-29T22-24-44.png)

+++

Representing the unary function as a binary relation:

```
(1,1), (2,1)
(1,1), (2,2)
(1,1), (2,3)
(1,2), (2,1)
(1,2), (2,2)
(1,2), (2,3)
(1,3), (2,1)
(1,3), (2,2)
(1,3), (2,3)
```

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T17-00-54.png)
```

+++

![x](raster/2023-10-29T22-26-54.png)

+++

![x](raster/2023-10-29T22-27-16.png)

+++

## 3.2.5. Isomorphisms in a category

+++

![x](raster/2023-10-29T23-42-53.png)

+++

### Exercise 3.30.

+++

![x](raster/2023-10-29T23-43-16.png)

+++

For `1.`, define f⁻¹(2) = a, f⁻¹(1) = b, f⁻¹(3) = c. For `2.` in general there are same as the
number of permutations of the set (n!), in this case 6.

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T17-02-06.png)
```

+++

### Exercise 3.31.

+++

![x](raster/2023-10-29T23-44-04.png)

+++

The identity idₘ for the object m always has an inverse, namely itself (idₘ).

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T17-02-37.png)
```

+++

### Exercise 3.32.

+++

![x](raster/2023-10-29T23-45-24.png)

+++

The monoid in Example 3.13 is not a group because $s$ does not have an inverse.

Both the monoids in Example 3.18 are a group. The first is a group because the inverse of $z$ is $z$,
and the inverse of $s$ is $s$. The second is a group because the inverse of $z$ is $z$, the inverse of $s$ is
$s;s;s$, the inverse of $s;s$ is $s;s$, and the inverse of $s;s;s$ is $s$.

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T17-05-05.png)
```

+++

### Exercise 3.33.

+++

![x](raster/2023-10-29T23-45-48.png)

+++

Consider the graph with only vertices m and n, and with arrows in both directions between m and n
(call them f and g). It is not the case that f;g = idₘ and g;f = idₙ; these are additional equations
that would violate the condition of this being a free group. Rather, f;g and g;f are additional
morphisms in the hom-sets hom(f,g) and hom(g,f) (which are infinite sets).

+++

```{admonition} Reveal 7S answer
:class: dropdown
![x](raster/2023-11-04T17-06-54.png)
```

+++

![x](raster/2023-10-29T23-47-42.png)
