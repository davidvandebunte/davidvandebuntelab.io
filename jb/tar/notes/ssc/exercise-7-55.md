---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Exercise 7.55

+++

![x](raster/2023-11-24T15-41-06.png)

+++

Answering via a drawing:

+++

![x](exercise-7-55.svg)
