# Set subgoal

Given a single goal and a less-than-fully-specified plan, expand it into more detailed subplans.
That is, should you take the time to produce this graph:

```
* b: G1
* ab: G2
* a
```

From this graph?

```
* b: G1
* a
```

In the language of [Types of Plans in Business: Breadth, Time-frame, Specificity & Frequency](
https://edukedar.com/types-of-plans/), increase the specificity of the plan.

# Value

Decrease uncertainty on the weight of the original plan. That is, increase the accuracy of both the
value and cost estimates on the plan, leading to an update in the weight.

Get down to a point where you can start working on the goal (a reasonable first step).

# Cost

## Learn faster

We use small tasks to help develop an understanding of the bigger picture (e.g. improve how fast we
get feedback, or incrementally improve notes). These tasks often unfortunately only have
instrumental value (there is no increase in value from hitting them as goals) and so there is little
value in writing them down unless someone else will audit your plan. For example, typing the letter
`t` into a plain text file (with e.g. python source code) is probably not worth including in your
plan.

## Think small

To force yourself into thinking in greater detail, explicitly consider some of the smallest steps
you could take. For example, you can literally think the steps of your morning routine to try to
improve it; how many steps do you take before you ready to start working at your computer? Do you
ever walk back and forth between two places? Or do you carry what you need with you?

At your computer you can consider all the way down to how you e.g. open and close tabs (browser,
text editor, tmux), when you use your mouse vs. your keyboard, and how often you need to look at the
keyboard when you type.

% See:
% - edit-plain-text.md
% - close-window.md
