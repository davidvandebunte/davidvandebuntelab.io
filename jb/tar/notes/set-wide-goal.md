# Set wide goal

Part of [](./explore-domain.md). Thinking "wide" makes you more of a renaissance man and less of a
specialist. What opportunities are you missing in life? See also the V in [INVEST (mnemonic)](
https://en.wikipedia.org/wiki/INVEST_(mnemonic)).

Planning widely is also about diversification. Is it wise to put all your eggs in one basket and
watch it closely? There's more than one way to make money (or achieve value, in general).

If you're doing more focused research (searching), you can see setting a wide goal as BFS. Is it
likely you're going to be able to find an alternative solution, or are you ignoring hard but likely
necessary questions? If your target is less negotiable, your solution space will be smaller, and you
shouldn't be looking as widely. Analogizing to physical exploration, you can see a wide goal as
adding to your map (reducing fog of war) in a BFS manner.

# Test

Should you take the time to add G2 to this graph?

```
* c: G1
| * b: G2
|/
* a
```

# Estimate cost

See [](./set-goal.md).
