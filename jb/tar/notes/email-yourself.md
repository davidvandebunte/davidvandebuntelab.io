# Email yourself

## Ideal scheduling

In many cases, you only need to see a reminder once (it's a one-time task). You don't want to forget
to do it, but it's not ideal to do it while you're in focused work. It may have a deadline, but it's
an easy deadline to hit so you can afford to schedule it to a time where you aren't in focused work
(the evenings, car rides, etc.). Some tasks you want to snooze to a point where you're talking with
someone else (e.g. a spouse, friend, or child) who could take on the task for you. Ironically, you
don't want this queue to ever run dry. If so, you may not be using your time efficiently.

It would be nice to be able to drag and drop emails to sort them priority as you start working on
them. Snoozing essentially allows this; it's a list of emails you can easily sort but using a date.
The date you should assign is the date that you *must* address the item, taking away from focused
work time. Sometimes the way the list would end up if you sorted by priority is different than it
would if you sorted by deadline, but usually this is the same.

Only snooze personal emails to the evenings (5 PM) and work emails to the morning (8 AM). You aren't
going to address personal emails in the morning or work emails at night, anyways. This lets you look
at every email once a day rather than twice.

## Uncertain repeating events

Some events happen at an at an unpredictable interval and an email-to-self is a good way to add
something to your TODo list on a set day. Still, these should become a link to an editable document
if they become established.

Sometimes you want to continue to experiment with some way of working, but aren't sure that your
approach is going to last long term or even for a day. It's not worth setting up a calendar
reminder or trying to prioritize the habit in a long list when you don't even know if it will work;
once you've proven the approach works you will either have memorized it or can set up a reminder for
farther off / regularly. Other times, you want a record of how experiments failed. You can always
put the reminder at the top of a "learn" document and email that to yourself.

## Forced deletion

When would you ever delete anything from regular reminders? When you've memorized the thoughts. This
is like any document not related to habits; you don't write a lot on topics you feel are obvious. An
email reminder has the advantage of being easy to archive when the thoughts are somewhat unorganized
and you feel that you have memorized them sufficiently. You are less prone to delete your notes and
habit development ideas. That is, you are forced to delete or continue to pay (in snoozing) for not
deleting your notes.

It's possible to see this as both an advantage and a disadvantage. In a world before writing and
reminders it wouldn't have been possible for your past self to impose on your present like this;
you'd naturally forget about unimportant notes. You should only delete notes when there's an
associated cost in readability or searches; see [Pros and cons of deleting unused code? - SO](
https://stackoverflow.com/a/47824954/622049).

That is, you should probably only be deleting notes when you've decided to organize them, on a
larger scale. What if your past self isn't right about what your current priorities should be?

# Cost

If you do email yourself, send a title-only email to start and then create a long draft reply you
never send. There's no reason to hit send; you can leave all secondary emails as a draft because you
can snooze the first email and the secondaries are connected. The major advantage is you can edit
content going all the way back to the original email. It's also easier to copy and paste the content
to plain text. Hitting "send" is the equivalent of a commit with git. Hit esc to get out of text
editing mode.

## Trips

It's better to email yourself than to risk waiting until you're at your desktop and can write your
thought down. On trips, it's almost better to email yourself than to wait until you can get to your
laptop, turn it on, start Ubuntu, and change then push your plain text notes (you tend to need to
push your notes on trips so you don't forget you have notes on your laptop).

A better alternative to emailing yourself is to email yourself a Google Doc where you can take
notes. A Google Doc can be easily organized (if you have extra time on the trip) and (once it's
open) is easier to append to with voice-to-text commands when you're driving. Because it's connected
to a snoozed email, you'll see it and convert the notes when the trip is done. See also comments
about barriers to entry in [](./convert-to-plain-text.md). Do not take notes in this document that
have time-sensitivity of any kind (email yourself instead).

% See:
% - https://mail.google.com/mail/u/0/#inbox/KtbxLvhRWJlZbKtJdwglcxZBmKzMcdKhGq

Emails often build up on trips and you don't when you're going to be able to get away from your
email and into your notes; you often snooze emails to trips making this even harder. Moving notes
may be less important than working on some task in email.

## Open Ended

An email to yourself doesn't need to be categorized into a document; you can start writing. This
makes it easier to get your thought out fast. Your follow-up thoughts can also be completely
unrelated to the original thought in a kind of stream of consciousness note. You aren't saving that
much time by avoiding one copy and paste of these thoughts to a random document, and you should
perhaps wait to do this until you're done brainstorming. First and foremost, focus on getting your
thoughts out, and organize later. How hard is it to choose a random document, though? It can be any
"learn" document or a document only tangentially related to your thoughts.

% See:
% - https://mail.google.com/mail/u/0/#inbox/KtbxLvhRWJlZbKtJdwglcxZBmKzMcdKhGq

## Tasks with date dependencies

It's easier to email yourself and snooze than to add a calendar event. Still, how hard is it to make
a calendar reminder? Many calendar events are one-time. You simply use the calendar tool less, and
it can take an extra click or finger tap. For these kind of tasks, you should be using
[](./add-calendar-event.md).

Your snoozed emails are the list of where you look for extra tasks when you are e.g. in the car,
have time in the evening on your phone, are on vacation, or are in a meeting. If they have a bunch
of tasks you must wait until a certain date to take care of then the list is hard to go through. To
make it easier to skim through your emails in this situation, use labels.

Add a "Work" label to help you avoid tasks you don't need to do on personal time. Add a "With
spouse" label for tasks that would be easier with your spouse (even if they do not necessarily
require them). Add an "At desktop" tag for tasks you can only do at your computer, such as edits to
plain text notes or using GIMP or Inkscape.

% TODO: Really, all of these are similar to your "Free eyes" and "Free ears" documents.

Not all labels need to be for scheduling; you may keep some around to help you group emails before
moving them to plain text (e.g. when you have many emails from only having your phone but many
thoughts).

## Regular decision cost

It may seem appropriate to decide how long to snooze a habit development reminder every time you
read it, but this is an extra decision that can wear you down (you only get so many decisions a
day).

## Haste

Don't take notes to self in your email; it establishes a feeling of a rush that you actually need to
kill. Take the time to commit your existing plain text notes, then add your notes there. You're also
exposing yourself to interruptions. You need to slow down your thinking (thinking fast and slow) in
order to actually stop thinking about the subject. To do that, it's going to be best practice to
take notes into plain text.

## Extra reading

Don't spend a lot of time reading long (unchangeable) email chains to yourself trying to figure out
what your lesson is. If these had started as plain text, you could have saved your history of
compressing the thoughts, and been able to compress earlier.

## Collection / Grouping

A simple alternative to an email to yourself is to add the thought to the top of an existing "learn"
document. Then at least within a particular topic you are able to sort the reminders by order of
importance; obviously your new idea is the most important. The downside to these documents is that
you can't control the frequency of the reminder; but it's not hard to send the link to yourself as
an "email to self" that is effectively editable (what you need most).

Why not have a single "learn habits" document you work on when you have an extra few minutes and
send to yourself every day? A list of reminders will force you to sort by priority, and keep a
stable and editable priority. Otherwise, the frequency of calendar reminders is effectively how you
set their priority relative to one another.

Grouping also encourages focused work on the topic. In some sense, sending an email to yourself is a
way to try to encourage focused work (connected thoughts) by reminding yourself to get back into
something important the next day. You often achieve the opposite effect when you pull yourself back
into thoughts that are old and disconnected with what you are currently working on.

An essential part of good prioritization is knowing when to keep a box shut, and when to look inside
it to refine your estimates. Looking into the box can take a huge amount of time. It's easy to
prioritize an email that is only a link to a document where the name is well compressed and
meaningful, because you often aren't as tempted to look inside it. It's harder to prioritize an
email that is a bunch of thoughts you are going to need to read; even getting the gist might require
reading a few sentences.

## Extended Distraction

You often end up taking notes into your email when you don't want to think about something; you want
to get back into focused work on software (e.g. thinking about process). It's like not being able to
stop thinking about something when you're trying to sleep. If the topic is work, or is personal and
would bother you, this is even more of a reason to take your notes in plain text (so you'll be able
to sleep at night).

Even if it's not (e.g. something in your family life) you often have unavoidable follow-up thoughts
soon after your first thought/idea. If you don't take notes in plain text, you won't be able to edit
your thoughts after writing down the first. You can try to snooze the first email to try to prevent
yourself from thinking about it, but you usually have another thought you need to add it soon after.
So you have to go and find it among snoozed emails to keep the thoughts grouped.
