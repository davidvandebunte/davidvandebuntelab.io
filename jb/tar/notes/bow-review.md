---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    Format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Review

The first 8-9 chapters of this book are well-written, and great on an audiobook. Near the end both
the audiobook and the book fall apart. Judea Pearl provides an errata you can rely on as it gets
bad; see [Book of Why - Errata Pages](
http://bayes.cs.ucla.edu/WHY/errata-pages-PearlMackenzie_BookofWhy_Final.pdf).

This review is based on the audiobook, print book, and:
- [The Book of Why, The New Science of Cause and Effect - Google Play Books](
https://play.google.com/books/reader?id=9H0dDQAAQBAJ&pg=GBS.PP1&printsec=frontcover).

See also:
- [Andrew Gelman's review](
https://statmodeling.stat.columbia.edu/2019/01/08/book-pearl-mackenzie/)

Related open-source material:
- [Mediation (statistics) - Wikipedia](https://en.wikipedia.org/wiki/Mediation_(statistics))
- [](./investigate-root-cause.md)
