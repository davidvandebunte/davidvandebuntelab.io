# Collect practice questions so others can check their work against yours, ideally providing
# feedback or answering your unresolved questions.
source("iplot.R")
suppressPackageStartupMessages(library(rethinking))

source("practice-deconfound-chp6-models.R")
source("practice-overfitting-underfitting.R")
source("practice-model-interactions.R")
source('practice-debug-mcmc-inference.R')
source("practice-model-counts.R")
source("practice-generalized-glms.R")
source("practice-multilevel-models.R")
source("practice-covarying-parameters.R")
source("practice-missing-imprecise.R")
source("practice-interpretable-models.R")
