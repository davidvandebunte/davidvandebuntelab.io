# Add calendar event

Some tasks you want to handle to when you are in focused work on a subject, such as taxes or open
enrollment. While you have a process for taxes you can put reminders in, sometimes you don't have a
process. That is, you only know a date when you'll be in focused work.

Some tasks have dependencies on actions by others that you can't control, but they have promised to
do at a certain time. Often, you want to simply check that e.g. your spouse did something that they
said they would do by a certain date. You can snooze these instead (see [](./email-yourself.md)
because you don't want to really force your spouse to do anything, but outsiders are often more
reliable (e.g. the government).
