---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Select license

+++

Selecting a license may be a trivial activity when it comes to notes. If you want to use any GPL
content (e.g. a picture of an assembly line in an article on performance optimization) then you also
have to release anything you have as GPL. Otherwise, you're going to be spending a lot of time
thinking about how to avoid GPL content. You can always relicense content later, removing the bits
that are GPL.

Why share everything GPL? If you can teach others something, you're making the world a better place,
and it may come back to you someday. Are you optimizing for global good? Or optimizing for the
chance you can live much longer because someone else can use your work to extend your lifetime
(perhaps in healthcare)? If you have enough money, help that person by giving them knowledge.
